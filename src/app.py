from post_controller import PostController
from timeloop import Timeloop
from datetime import timedelta

tl = Timeloop()
post_controller = PostController('./.env.json')


@tl.job(interval=timedelta(minutes=1))
def main():
    post_controller.prepare_and_search()


if __name__ == "__main__":
    main()
    tl.start(block=True)
