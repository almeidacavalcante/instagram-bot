import os
import shutil
import json
import urllib.request as url_request
from datetime import datetime, timedelta
import requests
import time

from instapy_cli import client
from googleapiclient.discovery import build
import random
from helpers.datetime_helper import convert_timestamp

MSG_NO_IMAGES_RETURNED = 'The Google Custom Search didnt return any image'


class PostController:

    def __init__(self, config_json_file_url):
        self.config = self.load_data(config_json_file_url)
        self.USERNAME = self.config['USERNAME']
        self.PASSWORD = self.config['PASSWORD']
        self.API_KEY = self.config['API_KEY']
        self.SEARCH_ENGINE_ID = self.config['SEARCH_ENGINE_ID']
        self.DATA_JSON_URL = 'src/data.json'
        self.POSTS_FOLDER_URL = 'src/assets/posts'

    def load_data(self, file_name):
        with open(file_name) as file:
            return json.load(file)

    def save_data(self, data, file_name):
        with open(file_name, 'w') as file:
            json.dump(data, file, default=convert_timestamp)

    def _search(self, search_term, api_key, cse_id, **kwargs):
        print('SEARCH TERM: {}'.format(search_term))
        service = build('customsearch', 'v1', developerKey=api_key)
        res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
        if 'items' not in res:
            return False

        return res['items']

    def _save_images_and_populate_file(self, items):
        data = self.load_data(self.DATA_JSON_URL)

        temp_description = ' '.join(random.sample(data['hashtags'], k=30))
        publication_date = datetime.now()

        for item in items:
            if not self._is_blacklisted(item['link']):
                file_local_url = 'src/assets/posts/{}__{}.jpg'.format(
                    str(publication_date), random.random())

                data['posts'].append({
                    'image_url': file_local_url,
                    'original_url': item['link'],
                    'description': temp_description,
                    'publication_date': publication_date + timedelta(seconds=60)})

                publication_date = publication_date + timedelta(seconds=60)
                try:
                    url_request.urlretrieve(item['link'], file_local_url)
                    data['blacklist'].append(item['link'])
                    self.save_data(data, self.DATA_JSON_URL)
                except Exception as exception:
                    print(exception)

    def _is_blacklisted(self, link):
        data = self.load_data(self.DATA_JSON_URL)
        if link in data['blacklist']:
            return True

        return False

    def _reset_posts_data(self):
        data = self.load_data(self.DATA_JSON_URL)
        print(data)
        data['posts'] = []
        self.save_data(data, self.DATA_JSON_URL)
        self._clear_folder(self.POSTS_FOLDER_URL)

    def _clear_folder(self, folder):
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)

            except Exception as e:
                print(e)

    def post_on_instagram(self, image_url, description):
        with client(self.USERNAME, self.PASSWORD) as cli:
            cli.upload(image_url, description)

    def _publish_next_post(self):
        data = self.load_data(self.DATA_JSON_URL)
        if len(data['posts']) > 0:
            post = data['posts'].pop()

            try:
                self.post_on_instagram(post['image_url'], post['description'])
                self.save_data(data, self.DATA_JSON_URL)
                self._remove_file(post['image_url'])
            except Exception as e:
                print(e)

    def _remove_file(self, file_url):
        try:
            if os.path.isfile(file_url):
                os.unlink(file_url)
        except Exception as exception:
            print(exception)

    def prepare_and_search(self):
        data = self.load_data(self.DATA_JSON_URL)
        if len(data['posts']) > 0:
            self._publish_next_post()
        else:
            self._clear_folder(self.POSTS_FOLDER_URL)
            self._proceed_search_and_save()

    def _proceed_search_and_save(self):
        search_source = self.load_data('./src/search_terms.json')
        term1 = random.choice(search_source['terms'])
        term2 = random.choice(search_source['terms'])
        site = random.choice(search_source['sites'])

        search_term = '{} {} site:{}'.format(term1, term2, site)
        items = self._search(
            search_term, self.API_KEY, self.SEARCH_ENGINE_ID, num=10, searchType='image', imgSize='huge')

        if items == False:
            raise Exception(MSG_NO_IMAGES_RETURNED)

        try:
            self._save_images_and_populate_file(items)

        except Exception as ex:
            print(ex)
            self._reset_posts_data()
