from datetime import datetime


def convert_timestamp(item_date_object):
    if isinstance(item_date_object, (datetime,)):
        return item_date_object.timestamp()
